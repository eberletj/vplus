using System.Drawing;

public class NutcrackerNodes {
    public Color PixelColor { get; set; }
    public Point Model { get; set; }
    public int Sparkle { get; set; }
    public int BufX { get; set; }
    public int BufY { get; set; }
}
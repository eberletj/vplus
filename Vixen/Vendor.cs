using VixenPlus.Properties;

public static class Vendor {
    public static readonly string ProductDescription = Resources.ProductDescription;
    public const string ProductName = "Vixen+ {Beta}";
    public const string ProductURL = "http://www.vixenplus.com/";

    public const string ModuleAuthoring = "Artisan";
    public const string ModulePreview = "Rehersal";
    public const string ModuleManager = "Roadie";
    public const string ModuleScheduler = "Conductor";

    public const string DataExtension = ".vda";
    public const string GroupExtension = ".vgr";
    public const string MapperExtension = ".vmap";
    public const string ProfilExtension = ".pro";
    public const string ProgramExtension = ".vpr";
    public const string RoutineExtension = ".vir";
    public const string SequenceExtension = ".vix";

    public const string SupportURL = "http://www.diychristmas.org/vb1/forumdisplay.php?19-VixenPlus";

    public const string UpdateURL = "http://www.vixenplus.com/updates/";
    public const string UpdateFile = "/ver.xml";
}
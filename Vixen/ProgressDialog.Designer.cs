using System.ComponentModel;
using System.Windows.Forms;

internal partial class ProgressDialog {
    private IContainer components = null;

    #region Windows Form Designer generated code

    private Label labelMessage;
    private Panel panel1;


    private void InitializeComponent() {
        this.labelMessage = new System.Windows.Forms.Label();
        this.panel1 = new System.Windows.Forms.Panel();
        this.panel1.SuspendLayout();
        this.SuspendLayout();
        // 
        // labelMessage
        // 
        this.labelMessage.AutoSize = true;
        this.labelMessage.Location = new System.Drawing.Point(11, 29);
        this.labelMessage.Name = "labelMessage";
        this.labelMessage.Size = new System.Drawing.Size(54, 13);
        this.labelMessage.TabIndex = 0;
        this.labelMessage.Text = "Loading...";
        // 
        // panel1
        // 
        this.panel1.AutoSize = true;
        this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.panel1.Controls.Add(this.labelMessage);
        this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.panel1.Location = new System.Drawing.Point(0, 0);
        this.panel1.Name = "panel1";
        this.panel1.Size = new System.Drawing.Size(333, 72);
        this.panel1.TabIndex = 1;
        // 
        // ProgressDialog
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoSize = true;
        this.ClientSize = new System.Drawing.Size(333, 72);
        this.Controls.Add(this.panel1);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        this.Name = "ProgressDialog";
        this.ShowIcon = false;
        this.ShowInTaskbar = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.TopMost = true;
        this.panel1.ResumeLayout(false);
        this.panel1.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    protected override void Dispose(bool disposing) {
        if (disposing && (this.components != null)) {
            this.components.Dispose();
        }
        base.Dispose(disposing);
    }
}
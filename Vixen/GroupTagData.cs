using System.Drawing;

public class GroupTagData {
    public bool IsLeafNode { get; set; }
    public Color NodeColor { get; set; }
    public string UnderlyingChannel { get; set; }
    public string Zoom { get; set; }
}
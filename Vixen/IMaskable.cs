﻿public interface IMaskable
{
    byte[][] Mask { get; set; }
}
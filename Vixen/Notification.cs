﻿public enum Notification
{
    PreferenceChange,
    KeyDown,
    SequenceChange,
    ProfileChange
}
﻿public interface ITickSource
{
    int Milliseconds { get; }
}
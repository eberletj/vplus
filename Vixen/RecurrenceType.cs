﻿internal enum RecurrenceType
{
    None,
    Daily,
    Weekly,
    Monthly,
    Yearly
}
﻿public interface IPlugIn
{
    string Author { get; }

    string Description { get; }

    string Name { get; }
    string ToString();
}
﻿internal interface IScheduledObject : IExecutable
{
    int Length { get; }
}
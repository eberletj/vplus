﻿namespace Standard
{
    using System;

    public enum ModifierType : uint
    {
        At = 0x100000,
        Clear = 0x500000,
        Every = 0x400000,
        For = 0x200000,
        Over = 0x200000,
        Wait = 0x300000
    }
}


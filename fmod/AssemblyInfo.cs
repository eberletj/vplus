// Assembly fmod, Version 1.0.0.0

[assembly: System.Reflection.AssemblyVersion("0.0.1.180")]
[assembly: System.Reflection.AssemblyTitle("fmod")]
[assembly: System.Reflection.AssemblyDescription("FMOD interface for Vixen")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyCompany("")]
[assembly: System.Reflection.AssemblyProduct("VixenPlus")]
[assembly: System.Reflection.AssemblyCopyright("Copyleft 2013")]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Runtime.InteropServices.Guid("69a72273-4e43-4617-a8bf-0ce05435efe3")]
[assembly: System.Reflection.AssemblyFileVersion("0.0.1.180")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.DisableOptimizations | System.Diagnostics.DebuggableAttribute.DebuggingModes.EnableEditAndContinue | System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | System.Diagnostics.DebuggableAttribute.DebuggingModes.Default)]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]


﻿namespace RemoteClient
{
    using System;

    internal enum Ports
    {
        ExecutionClient = 0xa1bb,
        LocalClient = 0xa1bd,
        Server = 0xa1b9,
        ServerAutoConnect = 0xa1bc,
        Web = 0xa1ba
    }
}


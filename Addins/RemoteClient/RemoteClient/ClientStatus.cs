﻿namespace RemoteClient
{
    using System;

    public class ClientStatus
    {
        public ExecutionStatus ExecutionStatus;
        public int ProgramLength;
        public string ProgramName;
        public int SequenceLength;
        public string SequenceName;
        public int SequenceProgress;
    }
}


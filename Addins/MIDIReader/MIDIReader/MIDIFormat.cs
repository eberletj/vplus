﻿namespace MIDIReader
{
    using System;

    internal enum MIDIFormat
    {
        MultiSong = 2,
        MultiTrack = 1,
        None = -1,
        SingleTrack = 0
    }
}


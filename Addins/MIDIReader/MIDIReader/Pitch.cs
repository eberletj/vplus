﻿namespace MIDIReader
{
    using System;

    internal enum Pitch
    {
        Natural,
        Flat,
        Sharp
    }
}


﻿namespace FMOD
{
    using System;

    public enum DELAYTYPE
    {
        END_MS,
        DSPCLOCK_START,
        DSPCLOCK_END,
        MAX
    }
}


﻿namespace FMOD
{
    using System;

    public class ADVANCEDSETTINGS
    {
        public IntPtr ASIOChannelList;
        public int ASIONumChannels;
        public IntPtr ASIOSpeakerList;
        public int cbsize;
        public uint defaultDecodeBufferSize;
        public int eventqueuesize;
        public float HRTFFreq;
        public float HRTFMaxAngle;
        public float HRTFMinAngle;
        public int max3DReverbDSPs;
        public int maxADPCMcodecs;
        public int maxMPEGcodecs;
        public int maxPCMcodecs;
        public int maxXMAcodecs;
        public float vol0virtualvol;
    }
}


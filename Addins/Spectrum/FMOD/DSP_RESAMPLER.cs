﻿namespace FMOD
{
    using System;

    public enum DSP_RESAMPLER
    {
        NOINTERP,
        LINEAR,
        CUBIC,
        SPLINE,
        MAX
    }
}


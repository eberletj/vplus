﻿namespace FMOD
{
    using System;

    public enum DSP_FLANGE
    {
        DRYMIX,
        WETMIX,
        DEPTH,
        RATE
    }
}


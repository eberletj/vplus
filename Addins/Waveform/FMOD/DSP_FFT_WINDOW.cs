﻿namespace FMOD
{
    using System;

    public enum DSP_FFT_WINDOW
    {
        RECT,
        TRIANGLE,
        HAMMING,
        HANNING,
        BLACKMAN,
        BLACKMANHARRIS,
        MAX
    }
}


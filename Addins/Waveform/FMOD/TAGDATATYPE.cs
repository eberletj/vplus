﻿namespace FMOD
{
    using System;

    public enum TAGDATATYPE
    {
        BINARY,
        INT,
        FLOAT,
        STRING,
        STRING_UTF16,
        STRING_UTF16BE,
        STRING_UTF8,
        CDTOC
    }
}


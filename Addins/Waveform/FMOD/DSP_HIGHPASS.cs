﻿namespace FMOD
{
    using System;

    public enum DSP_HIGHPASS
    {
        CUTOFF,
        RESONANCE
    }
}


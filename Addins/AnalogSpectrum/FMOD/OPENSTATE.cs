﻿namespace FMOD
{
    public enum OPENSTATE
    {
        READY,
        LOADING,
        ERROR,
        CONNECTING,
        BUFFERING
    }
}


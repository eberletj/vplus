﻿namespace FMOD
{
    public class REVERB_CHANNELFLAGS
    {
        public const uint DEFAULT = 15;
        public const uint DIRECTHFAUTO = 1;
        public const uint ENVIRONMENT0 = 8;
        public const uint ENVIRONMENT1 = 0x10;
        public const uint ENVIRONMENT2 = 0x20;
        public const uint ROOMAUTO = 2;
        public const uint ROOMHFAUTO = 4;
    }
}


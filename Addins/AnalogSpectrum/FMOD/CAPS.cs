﻿namespace FMOD
{
    public enum CAPS
    {
        HARDWARE = 1,
        HARDWARE_EMULATED = 2,
        NONE = 0,
        OUTPUT_FORMAT_PCM16 = 0x10,
        OUTPUT_FORMAT_PCM24 = 0x20,
        OUTPUT_FORMAT_PCM32 = 0x40,
        OUTPUT_FORMAT_PCM8 = 8,
        OUTPUT_FORMAT_PCMFLOAT = 0x80,
        OUTPUT_MULTICHANNEL = 4,
        REVERB_EAX2 = 0x100,
        REVERB_EAX3 = 0x200,
        REVERB_EAX4 = 0x400,
        REVERB_I3DL2 = 0x800,
        REVERB_LIMITED = 0x1000
    }
}


﻿namespace FMOD
{
    public enum SOUND_TYPE
    {
        UNKNOWN,
        AIFF,
        ASF,
        AAC,
        CDDA,
        DLS,
        FLAC,
        FSB,
        GCADPCM,
        IT,
        MIDI,
        MOD,
        MPEG,
        OGGVORBIS,
        PLAYLIST,
        S3M,
        SF2,
        RAW,
        USER,
        WAV,
        XM
    }
}

